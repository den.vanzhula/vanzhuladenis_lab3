﻿// Project3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

using namespace std;

class SqEqual
{
private:

	double a;
	double b;
	double c;

	double x1;
	double x2;

	bool Solution(void)
	{
		double d;


		if (a == 0)
			return false;

		d = b * b - 4 * a * c;

		if (d >= 0)
		{

			x1 = (-b - sqrt(d)) / (2 * a);
			x2 = (-b + sqrt(d)) / (2 * a);
		}
		else
			return false;

		return true;
	}

public:

	SqEqual(double a, double b, double c)
	{
		this->a = a;
		this->b = b;
		this->c = c;
	}


	bool GetSolution(double& x1, double& x2)
	{
		if (Solution()) // вызвать внутренний метод решения
		{
			x1 = this->x1;
			x2 = this->x2;
			return true; // есть решение
		}

		return false; // нету решения
	}

	bool GetSolutionP(double* x1, double* x2)
	{
		if (Solution())
		{
			*x1 = this->x1;
			*x2 = this->x2;

			return true;
		}
		return false;
	}

	bool operator()(double a, double b, double c, double* x1, double* x2)
	{
		this->a = a;
		this->b = b;
		this->c = c;

		if (Solution())
		{
			*x1 = this->x1;
			*x2 = this->x2;
			return true;
		}
		return false;
	}

	bool operator()(double& x1, double& x2)
	{

		if (Solution())
		{
			x1 = this->x1;
			x2 = this->x2;
			return true;

		}
		return false;
	}
};

void main(void)
{
	// перегрузка оператора () вызова функции
	double a = 2, b = -3, c = 1;
	SqEqual e(a, b, c);
	double x1, x2;
	bool f;

	// вызов операторной функции operator()() с двумя параметрами
	f = e(x1, x2);

	if (f)
	{
		cout << a << "* x^2 + (" << b << ") * x + (" << c << ") = 0" << endl;
		cout << "x1 = " << x1 << endl;
		cout << "x2 = " << x2 << endl << endl;
	}

	f = e(5, -7, 2, &x1, &x2);

	if (f)
	{
		cout << "6 * x^2 - 10*x + 3 = 0" << endl;
		cout << "x1 = " << x1 << endl;
		cout << "x2 = " << x2 << endl << endl;
	}
}